<?php

declare(strict_types=1);

namespace App\Tests\acceptance;

use App\Tests\AcceptanceTester;

final class HelloCest
{
    public function checkIGetStatus200(AcceptanceTester $i): void
    {
        $i->amOnPage('/');
    }
}
