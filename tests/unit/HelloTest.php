<?php

declare(strict_types=1);

namespace App\Tests\unit;

use PHPUnit\Framework\TestCase;

final class HelloTest extends TestCase
{
    /** @test */
    public function itRunsAUnitTest(): void
    {
        $this->assertEquals(1, 1);
    }
}
