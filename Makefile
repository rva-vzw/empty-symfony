.PHONY: qa deptrac php-cs-fixer fix psalm test-unit test-acceptance \
		phpstan test

qa: deptrac psalm phpstan

deptrac:
	./vendor/bin/deptrac --no-progress

php-cs-fixer:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer --dry-run -v fix

fix:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer -v fix

psalm:
	./vendor/bin/psalm --no-progress

test: tests-unit tests-acceptance

tests-unit:
	./vendor/bin/codecept run unit

tests-acceptance:
	./vendor/bin/codecept run acceptance

phpstan:
	php -d memory_limit=4G ./vendor/bin/phpstan analyse --no-progress