# Empty symfony project with tools

I really enjoy programming with Symfony and PHP. A lot of tools are available,
which is a joy, but also a burden. Because when I start a new project, it
always takes a lot of time before I have all my favorite tools set up and
running.

So I decided to create a repository with just an empty Symfony 6.2 project,
with all the tools I like to use.

The project will probably be out of date when I'll need it again. But hey,
maybe looking through the git logs may help me out setting up a new one 😄

## What's in it:

It's a [Symfony](https://symfony.com) 6.2 skeleton on php 8.2,
with `.gitlab-ci.yml` and `docker-compose.yml` configuration files.

I installed my favorite dev tools, that will automatically run when
you push to gitlab:

* [codeception](https://codeception.com) with a working unit test (phpunit) 
  and a working acceptance test.
  Run `make test` (with docker-composer running) to run the tests.
* [deptrac](https://github.com/qossmic/deptrac) with a very basic
  configuration, that you should adapt to your needs. 
* [psalm](https://psalm.dev), running at
  [errorlevel 1](https://psalm.dev/docs/running_psalm/error_levels/)
* [php-cs-fixer](https://github.com/PHP-CS-Fixer/PHP-CS-Fixer). To
  format your code automatically, run `make fix`.
* [phpstan](https://phpstan.org/), checking your code for
  [level 9](https://phpstan.org/user-guide/rule-levels).
* [local-php-security-checker](https://gitlab.com/savadenn-public/runners/local-php-security-checker)

Except for the security checker, `make` targets are provided for
all of these tools. `make qa` runs all static analysers.

## What's not in it:

* integration tests. Creating those, should be easy enough, just inherit
  from `KernelTestCase`. Use `KernelTestCase::getContainer()` to instantiate
  the services you want to run integration tests on.
* a database. Because that's only an implementation detail 😉