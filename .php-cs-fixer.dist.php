<?php

$config = new PhpCsFixer\Config();

return $config->setRules([
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
	    'phpdoc_to_comment' => false,
    ])->setFinder(
        PhpCsFixer\Finder::create()
            ->in([
                'src',
                'tests/unit',
                'tests/functional',
                'tests/acceptance',
            ])
            ->exclude([
                'src/Migrations',
            ])
    );
